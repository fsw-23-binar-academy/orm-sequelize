const articleRepo = require("../repositories/articleRepo");
class ArticleService {
  async articleGetAll() {
    const options = {
      where: {
        deletedAt: null,
        approved: true,
      },
      attributes: ["id", "title", "body", "createdAt"],
    };
    return await articleRepo.articleGetAll(options);
  }
  async articleGetByUser(userId) {
    const options = {
      where: {
        userId,
        deletedAt: null,
      },
      attributes: ["id", "title", "body", "createdAt"],
    };
    return await articleRepo.articleGetAll(options);
  }

  async articleFindOne({ articleId }) {
    const options = {
      where: {
        id: articleId,
      },
    };
    return await articleRepo.articleFindOne(options);
  }

  async articleCreate({ title, body }) {
    let [err, article] = await articleRepo.articleFindOne({
      where: {
        title,
      },
    });
  }
  async articleUpdate({ articleId, title, body, user }) {
    let [err, article] = await articleRepo.articleFindOne({
      where: {
        title,
      },
    });

    let [errFound, articleUser] = await articleRepo.articleFindOne({
      where: {
        id: articleId,
      },
    });
    if (articleUser.userId !== user.id) {
      console.log("bukan punya lo");
      err = "This article is not yours";
    }
    if (article) {
      err = "Title Already Exist";
    } else {
      await articleRepo.articleUpdate(
        {
          body: body,
          title: title,
        },
        {
          where: {
            id: articleId,
          },
        }
      );
    }
    return [err, article];
  }
}

const articleService = new ArticleService();

module.exports = articleService;
