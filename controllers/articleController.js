const Model = require("../models");
const { Account, Article, Comment } = Model;
const articleService = require("../services/articleService");
class ArticleController {
  async articleGetAll(req, res) {
    const [err, data] = await articleService.articleGetAll();
    console.log("err", err);
    console.log("data", data);
    if (err) {
      res.status(400).json({
        message: "error",
        err,
      });
    } else {
      res.json({
        message: "Get All Article",
        data,
      });
    }
  }

  async articleGetByUser(req, res) {
    const [err, data] = await articleService.articleGetByUser(
      req.params.userId
    );
    console.log("err", err);
    if (err) {
      res.status(400).json({
        message: "error",
        err,
      });
    } else {
      res.json({
        message: "Get Articles By User",
        data,
      });
    }
  }

  async articleFindOne(req, res) {
    const [err, data] = await articleService.articleFindOne({
      articleId: req.params.id,
    });
    console.log("err", err);
    if (err) {
      res.status(400).json({
        message: "error",
        err,
      });
    } else {
      res.json({
        message: "Get Article One",
        data,
      });
    }
  }

  async articleCreate(req, res) {}

  async articleUpdate(req, res) {
    const [err, data] = await articleService.articleUpdate({
      articleId: req.params.id,
      title: req.body.title,
      body: req.body.body,
      user: req.user,
    });

    if (err) {
      res.status(400).json({
        message: "error",
        err,
      });
    } else {
      res.json({
        message: "Succesfully update data",
        data,
      });
    }
  }

  async articleDelete() {}
}

const articleController = new ArticleController();

module.exports = articleController;
