const express = require("express");
const router = express.Router();
const Model = require("../models");
const { Account, Article, Comment } = Model;
/* GET janken list item. */
var jwt = require("jsonwebtoken");
router.post("/register", (req, res) => {
  Account.create({
    username: req.body.username,
    password: req.body.password,
    email: req.body.email,
  })
    .then(() => res.json({ message: "Succesfully created new user" }))
    .catch((err) => {
      res.status(400).json({
        message: err.message,
      });
    });
});

router.post("/login", async (req, res) => {
  const { username, password } = req.body;

  try {
    const isUserFound = await Account.findOne({
      where: {
        username,
        password,
      },
    });
    console.log("isUserFound", isUserFound);

    if (isUserFound) {
      jwt.sign(
        {
          username: isUserFound.username,
          email: isUserFound.email,
          id: isUserFound.id,
          isAdmin: isUserFound.admin
        },
        "secret_key",
        { expiresIn: 60 },
        (err, token) => {
          res.json({
            token: token,
            message: "Successfully login",
          });
        }
      );
    } else {
      res.json({
        message: "User Not Found",
      });
    }
  } catch (error) {
    //
    res.json({
      error,
    });
  }
});
router.get("/", (req, res) => {
  Account.findAll({
    include: [
      {
        model: Article,
      },
      {
        model: Comment,
      },
    ],
  })
    .then((data) => {
      res.json({
        message: "Testing",
        data,
      });
    })
    .catch((err) => {
      res.json({ message: "error", err });
    });
});

router.get("/:id", (req, res) => {
  Account.findOne({
    where: {
      id: req.params.id,
    },
    include: [
      {
        model: Article,
      },
      {
        model: Comment,
      },
    ],
  })
    .then((data) => {
      res.json({
        data,
      });
    })
    .catch((err) => {
      res.json({
        err,
      });
    });
});

module.exports = router;
