const express = require("express");
const router = express.Router();
const Model = require("../models");
const { Account, Article, Comment } = Model;
const articleController = require("../controllers/articleController");
const checkAuthentication = require("../middlewares/checkAuthentication");
// import { client } from "../cached/redis";
/* GET janken list item. */

// localhost:3000/articles/
// https://sequelize.org/docs/v6/core-concepts/model-querying-finders/#findandcountall

router.get("/", articleController.articleGetAll);
//localhost:3000/articles/2
router.get("/:id", articleController.articleFindOne);

router.get("/users/:userId", articleController.articleGetByUser);

router.post("/", checkAuthentication, async (req, res) => {
  try {
    const user = req.user;
    const articleCreated = await Article.create({
      title: req.body.title,
      body: req.body.body,
      userId: user.id,
    });
    res.json({
      message: "Succesfully create new article",
      articleCreated,
    });
  } catch (err) {
    res.json({
      message: err.message,
    });
  }
});

router.put("/:id", checkAuthentication, articleController.articleUpdate);

module.exports = router;
