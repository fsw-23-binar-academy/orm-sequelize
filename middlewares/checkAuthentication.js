const jwt = require("jsonwebtoken");

const checkAuthentication = (req, res, next) => {
  const token = req.headers.token;
  if (!token) {
    res.status(401).json({
      message: "session expired",
    });
  } else {
    jwt.verify(token, "secret_key", function (err, decoded) {
      if (err) {
        return res.json({
          message: "Token not valid",
        });
      }
      req.user = decoded;
      next();
    });
  }
};

module.exports = checkAuthentication;
