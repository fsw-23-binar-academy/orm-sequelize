const Model = require("../models");
const { Account, Article, Comment } = Model;

class ArticleRepo {
  async articleGetAll(options) {
    let err = null;
    try {
      const articles = await Article.findAll(options);
      return [err, articles];
    } catch (error) {
      let err = error;
      // silent error
      return [err, null];
    }
  }
  async articleFindOne(options) {
    let err;
    try {
      const article = await Article.findOne(options);
      return [err, article];
    } catch (error) {
      let err = error;
      // silent error
      return [err, null];
    }
  }

  async articleUpdate(options) {
    let err;
    try {
      const articleUpdated = await Article.update(options);
      return [err, articleUpdated];
    } catch (error) {
      return [err, null];
    }
  }
}

const articleRepo = new ArticleRepo();

module.exports = articleRepo;
